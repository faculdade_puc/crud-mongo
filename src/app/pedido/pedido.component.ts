import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ToastComponent } from '../shared/toast/toast.component';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.css']
})
export class PedidoComponent implements OnInit {

Pedidos = [];
isLoading =  true;

pedido = {};
isEditing = false;

addPedidoForm: FormGroup;
cod = new FormControl('', Validators.required);
client = new FormControl(Validators.required);
data = new FormControl(Validators.required);


  constructor(private http: Http,
              private dataService:DataService,
              public toast: ToastComponent,
              private FormBuilder: FormBuilder) { }

  ngOnInit() {
    this.get();

    this.addPedidoForm = this.FormBuilder.group({
      cod: ['', Validators.required],
      client: ['', Validators.required],
      data: ['', Validators.required],
    });
  }

  get(){
    this.dataService.get("/pedidos").subscribe(
      (data)=> {this.Pedidos = data; console.log(data)},
      error => console.log(error),
      () => this.isLoading = false
    );
  }

  addPedido(){
    this.dataService.add("/pedidos", this.addPedidoForm.value).subscribe(
      res => {
        const newPedido = res.json();
        this.Pedidos.push(newPedido);
        this.addPedidoForm.reset();
        this.toast.setMessage('Pedido Adicionado com Sucesso', 'success');
      },
      error => console.log(error)
    );
  }

enableEditing(pedido){
  this.isEditing = true;
  this.pedido = pedido;
}

cancelEditing(){
  this.isEditing = false;
  this.pedido = {};
  this.toast.setMessage('Edição Cancelada.', 'warning');
  //reload the cats to reset the editing
  this.get();
}

editPedido(pedido){
  this.dataService.edit("/pedidos", pedido).subscribe(
    res => {
      this.isEditing = false;
      this.pedido = pedido;
      this.toast.setMessage('Pedido Editado com Sucesso', 'success');
    },
    error => console.log(error)
  );
}

deletePedido(pedido){
  if(window.confirm('Deseja mesmo deletar esse pedido?')){
    this.dataService.delete("/pedidos", pedido).subscribe(
      res => {
        const pos = this.Pedidos.map(elem => { return elem._id; }).indexOf(pedido._id);
        this.Pedidos.splice(pos, 1);
        this.toast.setMessage('Pedido Deletado com Sucesso', 'success');
      },
      error => console.log(error)
    );
  }
}

}





