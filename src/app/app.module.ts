import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { ClientsComponent } from './Clients/Clients.component';
import { DataService } from './services/data.service';

import { ToastComponent } from './shared/toast/toast.component';
import { ProductsComponent } from './products/products.component';
import { PedidoComponent } from './pedido/pedido.component';
import { OrderItemsComponent } from './order-items/order-items.component';

const routing : Routes = [
    { path: '',      component: ClientsComponent },
    { path: 'app/clientes', component: ClientsComponent },
    { path: 'app/produtos', component: ProductsComponent },
    { path: 'app/pedidos', component: PedidoComponent },
    { path: 'app/order-items/:orderId', component: OrderItemsComponent}
];

@NgModule({
  declarations: [
    AppComponent,   
    ToastComponent,
    ClientsComponent,
    ProductsComponent,
    PedidoComponent,
    OrderItemsComponent
],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(routing)
  ],
  providers: [
    DataService,
    ToastComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})

export class AppModule { }
