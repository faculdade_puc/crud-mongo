var express = require('express');
var path = require('path');
var morgan = require('morgan'); // logger
var bodyParser = require('body-parser');

var app = express();
app.set('port', (process.env.PORT || 3000));

app.use('/', express.static(__dirname + '/../../dist'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(morgan('dev'));

var mongoose = require('mongoose');
mongoose.connect('mongodb://charles:sandrogovernador@ds119368.mlab.com:19368/trabalho');
var db = mongoose.connection;
mongoose.Promise = global.Promise;

// Models
var Client = require('./client.model.js');
var Product = require('./product.model.js');
var Pedido = require('./pedido.model.js');
var OrderItem = require('./order-item.model.js');


db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('Connected to MongoDB');

    /*CLIENT*/
        // select all
        app.get('/clients', function(req, res) {
            Client.find({}, function(err, docs) {
                if (err) return console.error(err);
                res.json(docs);
            });
        });

        // count all
        app.get('/clients/count', function(req, res) {
            Client.count(function(err, count) {
                if (err) return console.error(err);
                res.json(count);
            });
        });

        // create
        app.post('/clients', function(req, res) {

            var obj = new Client(req.body);
            console.log("obj", obj);
            obj.save(function(err, obj) {
                if (err) return console.error(err);
                res.status(200).json(obj);
            });
        });

        // find by id
        app.get('/clients/:id', function(req, res) {
            Client.findOne({ _id: req.params.id }, function(err, obj) {
                if (err) return console.error(err);
                res.json(obj);
            });
        });

        // update by id
        app.put('/clients/:id', function(req, res) {
            Client.findOneAndUpdate({ _id: req.params.id }, req.body, function(err) {
                if (err) return console.error(err);
                res.sendStatus(200);
            });
        });

        // delete by id
        app.delete('/clients/:id', function(req, res) {
            Client.findOneAndRemove({ _id: req.params.id }, function(err) {
                if (err) return console.error(err);
                res.sendStatus(200);
            });
        });
    /*CLIENT*/

    /*PRODUCT*/
         //APIs
        //select all
        app.get('/products', function(req, res) {
            Product.find({}, function(err, docs) {
                if (err) return console.error(err);
                res.json(docs);
            });
        });

        //count all
        app.get('/products/count', function(req, res) {
            Product.count(function(err, count) {
                if (err) return console.error(err);
                res.json(count);
            });
        });

        //create
        app.post('/products', function(req, res) {

            var obj = new Product(req.body);
            console.log("obj", obj);
            obj.save(function(err, obj) {
                if (err) return console.error(err);
                res.status(200).json(obj);
            });
        });

        //find by id
        app.get('/products/:id', function(req, res) {
            Product.findOne({ _id: req.params.id }, function(err, obj) {
                if (err) return console.error(err);
                res.json(obj);
            });
        });

        //update by id
        app.put('/products/:id', function(req, res) {
            Product.findOneAndUpdate({ _id: req.params.id }, req.body, function(err) {
                if (err) return console.error(err);
                res.sendStatus(200);
            });
        });

        //delete by id
        app.delete('/products/:id', function(req, res) {
            Product.findByIdAndRemove({ _id: req.params.id }, function(err) {
                if (err) return console.error(err);
                res.sendStatus(200);
            });
        });
    /*PRODUCT*/

    /*PEDIDO*/
         //APIs
        //select all
        app.get('/pedidos', function(req, res) {
            Pedido.find({}, function(err, docs) {
                if (err) return console.error(err);
                res.json(docs);
            });
        });

        //count all
        app.get('/pedidos/count', function(req, res) {
            Pedido.count(function(err, count) {
                if (err) return console.error(err);
                res.json(count);
            });
        });

        //create
        app.post('/pedidos', function(req, res) {

            var obj = new Pedido(req.body);
            console.log("obj", obj);
            obj.save(function(err, obj) {
                if (err) return console.error(err);
                res.status(200).json(obj);
            });
        });

        //find by id
        app.get('/pedidos/:id', function(req, res) {
            Pedido.findOne({ _id: req.params.id }, function(err, obj) {
                if (err) return console.error(err);
                res.json(obj);
            });
        });

        //update by id
        app.put('/pedidos/:id', function(req, res) {
            Pedido.findOneAndUpdate({ _id: req.params.id }, req.body, function(err) {
                if (err) return console.error(err);
                res.sendStatus(200);
            });
        });

        //delete by id
        app.delete('/pedidos/:id', function(req, res) {
            Pedido.findByIdAndRemove({ _id: req.params.id }, function(err) {
                if (err) return console.error(err);
                res.sendStatus(200);
            });
        });
/*PEDIDO*/


    /* Order Itens (Itens de pedidos) */
    /* Recupera itens de acordo com o pedido */
    app.get('/orderItems/:orderId', function(req, res) {
        Product.findOne({ orderId: req.params.orderId }, function(err, obj) {
            if (err) return console.error(err);
            res.json(obj);
        });
    });

    /* Adiciona um item de pedido */
    app.post('/orderItems', function(req, res) {

        var obj = new OrderItem(req.body);
        console.log("obj", obj);
        obj.save(function(err, obj) {
            if (err) return console.error(err);
            res.status(200).json(obj);
        });
    });

    /* Pesquisa produto */
    app.get('/products/search/:query', function(req, res){
        Product.find({name: new RegExp(req.params.query, "ig")}, function(err, docs) {
            if (err) return console.error(err);
            res.json(docs);
        });
    });

    /* itens do pedido*/
    app.get('/order/:id/items', function(req, res){
        console.log(req.params.id);
        OrderItem.find({orderId: req.params.id}, function(err, docs) {
            if (err) return console.error(err);
            let filled = 0;
            for(let i in docs)
                Product.find({_id: docs[i].productId}, function(err, product){
                    docs[i].product = product;
                    console.log(product);
                    
                    if(++filled == docs.length)
                    res.json(docs);
                });
            
            console.log('returned');
            
        });
    });

    // delete by id
    app.delete('/orderItems/:id', function(req, res) {
        console.warn('ID => '+ req.params.id);
        OrderItem.findOneAndRemove({ _id: req.params.id }, function(err) {
            if (err) return console.error(err);
            res.sendStatus(200);
        });
    });

    // all other routes are handled by Angular
    app.get('/*', function(req, res) {
        res.sendFile(path.join(__dirname, '/../../dist/index.html'));
    });

    app.listen(app.get('port'), function() {
        console.log('Angular 2 Full Stack listening on port ' + app.get('port'));
    });


   
});

module.exports = app;