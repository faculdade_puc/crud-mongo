var mongoose = require('mongoose');

var pedidoSchema = mongoose.Schema({
    cod: String,
    client: String,
    data: String
});

var Pedido = mongoose.model('Pedido', pedidoSchema);

module.exports = Pedido;